import { Injectable, NgZone } from '@angular/core';
import { User } from "../services/user";
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  userData: any; // Save logged in user data

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,  
    public ngZone: NgZone // NgZone service to remove outside scope warning
  ) {    
    /* Saving user data in localstorage when 
    logged in and setting up null when logged out */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.afs.collection('users')
          .doc(user.uid) // change to the current user id 
          .get().subscribe((u)=>{ 
            this.userData = user;
            u.data().roles 
            this.userData.providerData[0]['roles'] = u.data().roles
            localStorage.setItem('user', JSON.stringify(this.userData));
            JSON.parse(localStorage.getItem('user'));
          })
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    })
  }
  isAdmin : boolean = false;
  isEditor : boolean = false;
  isSubscriber : boolean = false;
  roles : any[] = [];
  // Sign in with email/password
  SignIn(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.afs.collection('users')
        .doc(result.user.uid) // change to the current user id 
        .get().subscribe((u)=>{
            if(u.exists){
              if (u.data().roles.hasOwnProperty('admin') && u.data().roles.admin == true) {
                this.isAdmin = true;
                this.roles.push('admin')
              } 
              if (u.data().roles.hasOwnProperty('editor') && u.data().roles.editor == true) {
                this.isEditor = true;
                this.roles.push('editor')
              } 
              if (u.data().roles.hasOwnProperty('subscriber') && u.data().roles.subscriber == true) {
                this.isSubscriber = true;
                this.roles.push('subscriber')
              }
            }
            if (this.isAdmin == true) {
              this.ngZone.run(() => {
                this.router.navigate(['admin']);
              });
            } else {
              this.ngZone.run(() => {
                this.router.navigate(['/']);
              });
            }
        })
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  // Sign up with email/password
  SignUp(email, password) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        /* Call the SendVerificaitonMail() function when new user sign 
        up and returns promise */
        this.SendVerificationMail();
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
    .then(() => {
      this.router.navigate(['verify-email-address']);
    })
  }

  // Reset Forggot password
  ForgotPassword(passwordResetEmail) {
    return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
    .then(() => {
      window.alert('Password reset email sent, check your inbox.');
    }).catch((error) => {
      window.alert(error)
    })
  }
  rolesBool : any[] = []
  result : boolean
  // Returns true when user is looged in and email is verified
  public isLoggedIn(allowedRoles: string[]): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user !== null) {
      if (user.providerData[0].roles.hasOwnProperty('admin') && user.providerData[0].roles.admin == true) {
        this.isAdmin = true;
        this.roles.push('admin')
      } 
      if (user.providerData[0].roles.hasOwnProperty('editor') && user.providerData[0].roles.editor == true) {
        this.isEditor = true;
        this.roles.push('editor')
      } 
      if (user.providerData[0].roles.hasOwnProperty('subscriber') && user.providerData[0].roles.subscriber == true) {
        this.isSubscriber = true;
        this.roles.push('subscriber')
      }
    }
    
    if (allowedRoles == null || allowedRoles.length === 0) {
      return (user !== null && user.emailVerified !== false) ? true : false;
    } else {
      this.roles.forEach(e => {
        this.rolesBool.push(allowedRoles.includes(e));
      });
    }
    return (user !== null && user.emailVerified !== false && this.rolesBool.includes(true)) ? true : false;
  }

  // Sign in with Google
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  // Auth logic to run auth providers
  AuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
    .then((result) => {
       this.ngZone.run(() => {
          this.router.navigate(['/']);
        })
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error)
    })
  }

  /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      nom: null,
      prenom: null,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      IBAN: null,
      adresse_domicile: null,
      date_naissance: null,
      entreprise: null,
      num_immatricule_entreprise: null,
      roles: {
        subscriber: true
      }
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  // Sign out 
  SignOut() {
    return this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['sign-in']);
    })
  }

}