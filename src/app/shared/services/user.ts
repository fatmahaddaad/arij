export interface Roles { 
    subscriber?: boolean;
    editor?: boolean;
    admin?: boolean;
}
export interface User {
    uid: string;
    email: string;
    nom: string;
    prenom: string
    photoURL: string;
    emailVerified: boolean;
    IBAN: string;
    adresse_domicile: string;
    date_naissance: string;
    entreprise: string;
    num_immatricule_entreprise: string;
    roles: Roles;
 }