import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.loadScript('../../../assets/admin/js/lib/bootstrap/js/popper.min.js');
    this.loadScript('../../../assets/admin/js/lib/bootstrap/js/bootstrap.min.js');
    this.loadScript('../../../assets/admin/js/jquery.slimscroll.js');
    this.loadScript('../../../assets/admin/js/sidebarmenu.js');
    this.loadScript('../../../assets/admin/js/lib/sticky-kit-master/dist/sticky-kit.min.js');
    this.loadScript('../../../assets/admin/js/lib/morris-chart/raphael-min.js');
    this.loadScript('../../../assets/admin/js/lib/morris-chart/morris.js');
    this.loadScript('../../../assets/admin/js/lib/morris-chart/dashboard1-init.js');
	  this.loadScript('../../../assets/admin/js/lib/calendar-2/moment.latest.min.js');
    this.loadScript('../../../assets/admin/js/lib/calendar-2/semantic.ui.min.js');
    this.loadScript('../../../assets/admin/js/lib/calendar-2/prism.min.js');
    this.loadScript('../../../assets/admin/js/lib/calendar-2/pignose.calendar.min.js');
    this.loadScript('../../../assets/admin/js/lib/calendar-2/pignose.init.js');
    this.loadScript('../../../assets/admin/js/lib/owl-carousel/owl.carousel.min.js');
    this.loadScript('../../../assets/admin/js/lib/owl-carousel/owl.carousel-init.js');
    this.loadScript('../../../assets/admin/js/scripts.js');
    this.loadScript('../../../assets/admin/js/custom.min.js');
  }
  public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }
}
