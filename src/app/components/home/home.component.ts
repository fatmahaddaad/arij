import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';

import { AngularFirestore } from 'angularfire2/firestore';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(db: AngularFirestore) {
      this.items = db.collection('/users').valueChanges();
    }
  ngOnInit() {
  }

  public items: Observable<any[]>;
  
}
