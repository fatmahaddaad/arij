// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAIMN1LLhP7P3ZN-w-N8uJZ6Dr6Nt9ZCx8",
    authDomain: "arij-93830.firebaseapp.com",
    databaseURL: "https://arij-93830.firebaseio.com",
    projectId: "arij-93830",
    storageBucket: "arij-93830.appspot.com",
    messagingSenderId: "28506571668",
    appId: "1:28506571668:web:264cdb3203c16064"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
